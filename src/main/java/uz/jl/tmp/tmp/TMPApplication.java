package uz.jl.tmp.tmp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@EnableAspectJAutoProxy

public class TMPApplication {

    public static void main(String[] args) {
        SpringApplication.run(TMPApplication.class, args);
    }

}
