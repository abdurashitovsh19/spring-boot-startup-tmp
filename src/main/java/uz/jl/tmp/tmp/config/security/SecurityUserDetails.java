package uz.jl.tmp.tmp.config.security;

import lombok.Builder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uz.jl.tmp.tmp.domains.auth.AuthUser;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;



@Builder
public record SecurityUserDetails(AuthUser authUser) implements UserDetails {

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authUser.getUserRights()
                .stream()
                .map(authUserRight -> authUserRight.getRole().getAuthority())
                .distinct()
                .map(SimpleGrantedAuthority::new)
                .toList();
    }

    @Override
    public String getPassword() {
        return authUser.getPassword();
    }

    @Override
    public String getUsername() {
        return authUser.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return authUser().getStatus().notEquals(AuthUser.Status.ACCOUNT_EXPIRED);
    }

    @Override
    public boolean isAccountNonLocked() {
        return authUser.getStatus().equals(AuthUser.Status.ACTIVE);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return authUser.getStatus().notEquals(AuthUser.Status.CREDENTIALS_EXPIRED);
    }

    @Override
    public boolean isEnabled() {
        return authUser.isDeleted();
    }

    public Long getId() {
        return authUser.getId();
    }
}