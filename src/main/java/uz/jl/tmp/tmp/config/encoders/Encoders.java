package uz.jl.tmp.tmp.config.encoders;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.password.PasswordEncoder;



@Configuration
public class Encoders {

    @Value("${encryptor.secret}")
    private String secret;

    @Value("${encryptor.key}")
    private String key;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

//    @Bean
//    public TextEncryptor encryptors() {
//        return Encryptors.text(secret, key);
//    }

}
