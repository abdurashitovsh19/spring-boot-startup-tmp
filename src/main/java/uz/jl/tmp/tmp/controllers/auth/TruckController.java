package uz.jl.tmp.tmp.controllers.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.jl.tmp.tmp.domains.Truck;
import uz.jl.tmp.tmp.repositories.TruckRepo;

import java.util.List;

@RestController("/truck")
@RequiredArgsConstructor
public class TruckController {

    private final TruckRepo truckRepo;

    @PostMapping("/create")
    public Truck create(String name, String vid, String number, Long id) {


        Truck truck = new Truck();
        truck.setName(name);
        truck.setVid(vid);
        truck.setNumber(number);
        truck.setId(id);
        return truckRepo.save(truck);
    }

    @GetMapping("/all")
    public List<Truck> getAll() {
        return truckRepo.findAll();
    }
}
