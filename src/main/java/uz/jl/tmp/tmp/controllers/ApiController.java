package uz.jl.tmp.tmp.controllers;

import lombok.RequiredArgsConstructor;
import uz.jl.tmp.tmp.services.base.BaseService;


@RequiredArgsConstructor
public abstract class ApiController<S extends BaseService> {
    protected final S service;
    protected final String API = "/api";
    protected final String V1 = "/v1";
}
