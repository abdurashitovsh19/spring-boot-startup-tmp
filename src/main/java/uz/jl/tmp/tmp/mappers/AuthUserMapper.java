package uz.jl.tmp.tmp.mappers;

import org.mapstruct.Mapper;
import uz.jl.tmp.tmp.domains.auth.AuthUser;
import uz.jl.tmp.tmp.dtos.user.AuthUserCreateDTO;
import uz.jl.tmp.tmp.dtos.user.AuthUserDTO;
import uz.jl.tmp.tmp.dtos.user.AuthUserUpdateDTO;



@Mapper(componentModel = "spring")
public interface AuthUserMapper extends BaseMapper<AuthUser, AuthUserDTO, AuthUserCreateDTO, AuthUserUpdateDTO> {

}
