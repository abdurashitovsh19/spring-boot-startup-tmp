package uz.jl.tmp.tmp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.jl.tmp.tmp.domains.auth.AuthUser;

import java.util.Optional;


public interface AuthUserRepository extends JpaRepository<AuthUser, Long>, GenericRepository {
    Optional<AuthUser> findByUsername(String username);
}
