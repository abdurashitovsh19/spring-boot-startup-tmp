package uz.jl.tmp.tmp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.jl.tmp.tmp.domains.Truck;

@Repository
public interface TruckRepo extends JpaRepository<Truck, Long> {
}
