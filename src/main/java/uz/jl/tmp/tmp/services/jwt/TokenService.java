package uz.jl.tmp.tmp.services.jwt;


import uz.jl.tmp.tmp.config.security.SecurityUserDetails;


public interface TokenService {

    String generateToken(SecurityUserDetails userDetails);

    Boolean isValid(String token);


}
