package uz.jl.tmp.tmp.services.base;

import lombok.RequiredArgsConstructor;
import uz.jl.tmp.tmp.mappers.BaseMapper;
import uz.jl.tmp.tmp.mappers.GenericMapper;
import uz.jl.tmp.tmp.repositories.GenericRepository;
import uz.jl.tmp.tmp.utils.BaseUtils;


@RequiredArgsConstructor
public abstract class AbstractService<R extends GenericRepository, M extends GenericMapper> {
    protected final R repository;
    protected final M mapper;
    protected final BaseUtils utils;
}
