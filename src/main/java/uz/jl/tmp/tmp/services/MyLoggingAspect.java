package uz.jl.tmp.tmp.services;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
@Slf4j
public class MyLoggingAspect {

    @Pointcut("execution(* uz.jl.tmp.tmp.controllers.auth.TruckController.create())")
    public void serviceMethods() {
    }

    @Before("serviceMethods()")
    public void logMethodCall(JoinPoint joinPoint) {
        log.info("Method called: " + joinPoint.getSignature().getName());
    }
}
