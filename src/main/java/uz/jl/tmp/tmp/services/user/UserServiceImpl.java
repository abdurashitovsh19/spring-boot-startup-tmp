package uz.jl.tmp.tmp.services.user;

import com.sun.xml.bind.v2.TODO;
import lombok.NonNull;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.jl.tmp.tmp.criteria.AuthUserCriteria;
import uz.jl.tmp.tmp.domains.auth.AuthUser;
import uz.jl.tmp.tmp.dtos.user.AuthUserCreateDTO;
import uz.jl.tmp.tmp.dtos.user.AuthUserDTO;
import uz.jl.tmp.tmp.dtos.user.AuthUserUpdateDTO;
import uz.jl.tmp.tmp.mappers.AuthUserMapper;
import uz.jl.tmp.tmp.repositories.AuthUserRepository;
import uz.jl.tmp.tmp.services.base.AbstractService;

import uz.jl.tmp.tmp.utils.BaseUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class UserServiceImpl extends AbstractService<AuthUserRepository, AuthUserMapper> implements UserService {

    private final PasswordEncoder passwordEncoder;
    private final ServerProperties serverProperties;

    public UserServiceImpl(AuthUserRepository repository,
                           AuthUserMapper mapper,
                           BaseUtils utils,
                           PasswordEncoder passwordEncoder, ServerProperties serverProperties) {
        super(repository, mapper, utils);

        this.passwordEncoder = passwordEncoder;
        this.serverProperties = serverProperties;
    }

    @Override
    public List<AuthUserDTO> getAll(@NonNull AuthUserCriteria criteria) {
        return null;
    }

    @Override
    public AuthUserDTO get(@NonNull Long id) {
        return null;
    }

    @Override
    public Long create(@NonNull AuthUserCreateDTO dto) {
        AuthUser authUser = mapper.fromCreateDto(dto);
        authUser.setPassword(passwordEncoder.encode(authUser.getPassword()));
//        authUser.setCreatedBy(-1L);
//        authUser.setStatus(AuthUser.Status.NOT_ACTIVE);
        authUser.setStatus(AuthUser.Status.ACTIVE);
        repository.save(authUser);
//        sendActivationLink(authUser);
//        TODO send activation code here
        return authUser.getId();
    }

    @Override
    public void delete(@NonNull Long id) {

    }

    @Override
    public void update(@NonNull AuthUserUpdateDTO dto) {

    }
}
