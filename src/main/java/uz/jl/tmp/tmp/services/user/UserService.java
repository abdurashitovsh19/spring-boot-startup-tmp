package uz.jl.tmp.tmp.services.user;

import uz.jl.tmp.tmp.criteria.AuthUserCriteria;
import uz.jl.tmp.tmp.dtos.user.AuthUserCreateDTO;
import uz.jl.tmp.tmp.dtos.user.AuthUserDTO;
import uz.jl.tmp.tmp.dtos.user.AuthUserUpdateDTO;
import uz.jl.tmp.tmp.services.base.GenericCrudService;


public interface UserService extends GenericCrudService<
        Long,
        AuthUserDTO,
        AuthUserCreateDTO,
        AuthUserUpdateDTO,
        AuthUserCriteria> {
}
