package uz.jl.tmp.tmp.services.base;

import uz.jl.tmp.tmp.criteria.GenericCriteria;
import uz.jl.tmp.tmp.dtos.Dto;
import uz.jl.tmp.tmp.dtos.GenericDto;

import java.io.Serializable;


public interface GenericCrudService<ID extends Serializable,
        D extends GenericDto,
        CD extends Dto,
        UD extends GenericDto,
        C extends GenericCriteria> extends GenericService<ID, CD, UD>, GenericReadService<ID, C, D> {
}
