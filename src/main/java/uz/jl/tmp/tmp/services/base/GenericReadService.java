package uz.jl.tmp.tmp.services.base;

import lombok.NonNull;
import uz.jl.tmp.tmp.criteria.GenericCriteria;
import uz.jl.tmp.tmp.dtos.GenericDto;

import java.io.Serializable;
import java.util.List;


public interface GenericReadService<ID extends Serializable, C extends GenericCriteria, D extends GenericDto> extends BaseService {
    List<D> getAll(@NonNull C criteria);

    D get(@NonNull ID id);
}
