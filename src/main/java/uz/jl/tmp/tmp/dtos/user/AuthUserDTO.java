package uz.jl.tmp.tmp.dtos.user;

import uz.jl.tmp.tmp.domains.auth.AuthUser;
import uz.jl.tmp.tmp.dtos.GenericDto;


public class AuthUserDTO extends GenericDto {
    private String username;
    private AuthUser.Status status;
}
