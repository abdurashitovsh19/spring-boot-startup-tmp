package uz.jl.tmp.tmp.dtos.auth.request;


public record AccessTokenRequest(
        String username,
        String password) {
}
