package uz.jl.tmp.tmp.dtos.auth.request;


public record RefreshTokenRequest(String refreshToken) {

}
