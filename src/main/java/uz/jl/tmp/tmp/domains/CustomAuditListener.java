package uz.jl.tmp.tmp.domains;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.actuate.audit.AuditEventRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.stereotype.Component;
import uz.jl.tmp.tmp.repositories.TruckRepo;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Component

public class CustomAuditListener {


    // Public no-arg constructor


    public void addActivity(Truck truck) {

        System.out.println("truck = " + truck);
    }


//    @PreUpdate
//    public void beforeUpdate(Truck entity, Truck oldEntity) {
//        // Access both the old and new entity states
//        System.out.println("Truck updated: Old name: " + oldEntity.getName() + ", New name: " + entity.getName());
//    }
//

//
//    @PostUpdate
//    public void postUpdate(Object o) {
//        System.out.println("PostUpdate = " + o);
//
//    }
//
//    @PostPersist
//    public void postPersist(Object o) {
//        System.out.println("PostPersist = " + o);
//
//    }


}


