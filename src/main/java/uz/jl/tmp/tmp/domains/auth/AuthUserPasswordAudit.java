package uz.jl.tmp.tmp.domains.auth;

import lombok.*;
import uz.jl.tmp.tmp.domains.Auditable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AuthUserPasswordAudit extends Auditable {

    @Column(unique = true)
    private String oldPassword;

    @Column(nullable = false, unique = true)
    private String currentPassword;

    @ManyToOne
    private AuthUser authUser;

    @Builder(builderMethodName = "childBuilder")
    public AuthUserPasswordAudit(Long id, boolean deleted, LocalDateTime createdAt, Long createdBy, LocalDateTime updatedAt, Long updatedBy, String oldPassword, String currentPassword, AuthUser authUser) {
        super(id, deleted, createdAt, updatedAt, updatedBy);
        this.oldPassword = oldPassword;
        this.currentPassword = currentPassword;
        this.authUser = authUser;
    }
}
