package uz.jl.tmp.tmp.domains;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Optional;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Truck extends Auditable {

    private String name;
    private String vid;
    private String number;

    @Version
    private Long version;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;
    @JoinColumn(name = "truck_id", referencedColumnName = "id")
    @OneToOne
    private Truck previous;

    // Inject EntityManager using a suitable method (constructor injection or service layer)

    @PreUpdate
    public void trackChanges() {


        // Access EntityManager through method injection within the lifecycle callback

        System.out.println(this.name);
    }

    @PostPersist
    public void postPersist() {
        previous = Truck.builder()

                .name(this.name)
                .vid(this.vid)
                .number(this.number)
                .version(this.version)
                .build();

    }


}
